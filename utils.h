#ifndef UTILS_H
#define UTILS_H

#include <string.h>
#include <stdio.h>

/**
 * @brief print_data Prints content of the buffer into a given file
 * @param data buffer to be saved into a file
 * @param filename Path to the file in filesystem
 */

void print_data(unsigned char* data, const char* filename)
{
    FILE* f = fopen(filename, "w");
    size_t data_len = strlen(data);

    for (size_t i = 0; i < data_len; ++i)
    {
        fprintf(f, "%c", data[i]);
    }
    fclose(f);
}

/**
 * @brief extract_data Reads data from given file to a memory
 * @param data Buffer to be filled with data
 * @param filename Path to the file in filesystem
 */

void extract_data(unsigned char* data, const char* filename)
{
    FILE* f = fopen(filename, "r");
    unsigned int value;

    while ((value = fgetc(f)) != EOF)
    {
        if (data != NULL)
        {
            *data = (unsigned char)value;
            ++data;
        }
        else break;
    }

    fclose(f);
}

/**
 * @brief console_log_data Prints the content of buffer to the console
 * @param data Buffer to be printed
 */

void console_log_data(unsigned char* data)
{
    puts("\nLOG HERE:");
    while (*data != NULL)
    {
        printf("%3d ", *data);
        ++data;
    }
    puts("\n/ LOG ENDS\n\n");
}

#endif // UTILS_H
