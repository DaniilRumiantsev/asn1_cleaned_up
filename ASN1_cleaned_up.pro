TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CFLAGS += -std=c11 -Wall -pedantic -Wstrict-prototypes

SOURCES += main.c \
    asn1/bignum.c \
    asn1/parse.c \
    asn1/write.c \

HEADERS += \
    asn1/parse.h \
    asn1/bignum.h \
    asn1/write.h \
    asn1/bn_mul.h \
    simple.h \
    sequence.h \
    utils.h
