#ifndef SIMPLE_H
#define SIMPLE_H

#include "asn1/parse.h"
#include "asn1/write.h"
#include "utils.h"

#define FILE_NAME "simple.txt"

static unsigned char simple_data[200] = {0};
static unsigned char simple_parsed_data[200] = {0};

void run_simple_examples(void)
{
    unsigned char* start = simple_data;
    unsigned char* end = start + 200; // Current position in the chars array


    /**
     * WRITE
     * ASN1_INTEGER AND ASN1_STRING
     */

    // Values

    int num = 67000;
    const char* text = "Here is the simple text to be written and parsed again";

    // Write num
    asn1_write_int(&end, start, num);

    // Write text
    size_t text_len = strlen(text);
    asn1_write_printable_string(&end, start, text, text_len);

    // Write data to local file
    print_data(end, FILE_NAME);

    /**
     * PARSE
     * ASN1_INTEGER AND ASN1_STRING
     */

    unsigned char* parsed_data_start = simple_parsed_data;
    unsigned char* parsed_data_end = parsed_data_start + 200;
    extract_data(parsed_data_start, FILE_NAME);

    // Parse string
    char* parsed_text = calloc(text_len, 1);
    // TODO: Find out why malloc(text_len) gives 5 bytes more than needed???
    // char* parsed_text = malloc(text_len);
    //printf("text_len: %d, strlen of parsed_text: %d\n", text_len, strlen(parsed_text));
    asn1_get_printable_string(&parsed_data_start, parsed_data_end, parsed_text);

    // Parse num
    int val;
    asn1_get_int(&parsed_data_start, parsed_data_end, &val);

    printf("SIMPLE TAGS\n___________\n\nNumber: %d\nText: %s\n", val, parsed_text);
}

#endif
