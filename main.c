/**
  * Demonstrates writing rimitive types (ASN_INTEGER and ASN1_PRINTABLE_STRING)
  * to ASN-1 format and parsing them back.
  * Based on mbedtls 2.9.0 ANS-1 implementation (https://tls.mbed.org)
  */

#include "simple.h"
#include "sequence.h"

int main(void)
{
    srand(time(NULL));

    run_simple_examples();
    run_sequence_example();

    return 0;
}
