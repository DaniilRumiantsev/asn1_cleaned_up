#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "asn1/parse.h"
#include "asn1/write.h"
#include "utils.h"

#define NUMS_TOTAL 10
#define FILE_NAME "sequence.txt"

static unsigned char sequence_data[200] = {0};
static unsigned char sequence_parsed_data[200] = {0};

void run_sequence_example(void)
{
    // Write four nums in ASN1 format

    unsigned char* start = sequence_data;
    unsigned char* end = start + 200;

    // Define total size for parsing a sequence
    int sequence_size = 0;

    puts("\n\nSEQUENCE\n________\n\nGenerated numbers:\n");

    for (int i = 0; i < NUMS_TOTAL; ++i)
    {
        int val = rand()%65536;
        printf("%6d ", val);
        asn1_write_int(&end, start, val);

        // Here we generate only 1-byte or 2-byte numbers
        sequence_size += 2 + (val > 255 ? 2 : 1);
    }

    asn1_write_len(&end, start, sequence_size);
    asn1_write_tag(&end, start, ASN1_SEQUENCE | ASN1_CONSTRUCTED);

    // Save data to a file
    print_data(end, FILE_NAME);

    // Parse data from file
    unsigned char* parsed_data_start = sequence_parsed_data;
    unsigned char* parsed_data_end = parsed_data_start + sequence_size + 2;
    extract_data(parsed_data_start, FILE_NAME);

    // Create sequence of ASN-1 elements
    asn1_sequence s;
    asn1_get_sequence_of(&parsed_data_start, parsed_data_end, &s, ASN1_INTEGER);

    // Print sequence

    puts("\n\nExtracted numbers:\n");

    asn1_sequence* item = &s;

    while (item != NULL)
    {
        int val = 0;

        for (size_t i = 0; i < item->buf.len; ++i) val = ( val << 8 ) | *item->buf.p++;

        printf("%6u ", val);
        item = item->next;
    }

    puts("\n");
}

#endif // SEQUENCE_H
